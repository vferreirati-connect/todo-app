import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/bloc/list_page_bloc.dart';
import 'package:todo_app/bloc/list_page_state.dart';
import 'package:todo_app/pages/editor_page.dart';
import 'package:todo_app/widgets/todo_listview.dart';

class ListPage extends StatefulWidget {
  static String routeName = "/";

  @override
  State createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  ListPageBloc bloc;

  Widget _addTodoButton() {
    return IconButton(
      icon: Icon(Icons.add, color: Colors.white),
      onPressed: () => Navigator.of(context).pushNamed(EditorPage.routeName)
    );
  }

  Widget _emptyView() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.note, color: Colors.grey, size: 80),
          SizedBox(height: 10),
          Text("All done in here.", style: TextStyle(color: Colors.grey)),
          Text("Try creating a new to-do!", style: TextStyle(color: Colors.grey))
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    bloc = BlocProvider.of<ListPageBloc>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("To-do List"),
        actions: <Widget>[
          _addTodoButton()
        ],
      ),
      body: BlocBuilder(
          bloc: bloc, 
          builder: (context, ListPageState state) {
            if(state.isLoading) {
              return Center(child: CircularProgressIndicator());

            } else if(state.todos != null && state.todos.length > 0) {
              return TodoListView(state.todos);

            } else {
              return _emptyView();
            }
          }
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    bloc.dispose();
  }
}
