import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/bloc/editor_page_bloc.dart';
import 'package:todo_app/bloc/editor_page_state.dart';

class EditorPage extends StatefulWidget {
  static String routeName = "/editor";

  @override
  _EditorPageState createState() => _EditorPageState();
}

class _EditorPageState extends State<EditorPage> {
  EditorPageBloc bloc;
  TextEditingController titleController;
  TextEditingController descriptionController;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<EditorPageBloc>(context);
    titleController = TextEditingController();
    descriptionController = TextEditingController();

    if(bloc.isEditingMode) {
      bloc.editingTodo.listen((todo) {
        titleController.text = todo.title;
        descriptionController.text = todo.description;
      });
    }
  }

  Widget _titleField(String error) {
    return TextField(
      controller: titleController,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        labelText: "Title*",
        errorText: error,

      ),
      onChanged: (value) => bloc.onUpdateTitle(value),
    );
  }

  Widget _descriptionField() {
    return TextField(
      controller: descriptionController,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        labelText: "Description"
      ),
      onChanged: (value) => bloc.onUpdateDescription(value),
    );
  }

  Widget _saveButton() {
    return IconButton(
      icon: Icon(Icons.save, color: Colors.white),
      onPressed: _onSave,
    );
  }

  void _onSave() async {
    var inserted = await bloc.onSave();
    if(inserted) {
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("To-Do Editor"),
        actions: <Widget>[
          _saveButton()
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: BlocBuilder(
          bloc: bloc,
          builder: (context, EditorPageState state) {
            return Column(
              children: <Widget>[
                _titleField(state.todoTitleError),
                _descriptionField(),
              ],
            );
          },
        ),
      ),
    );
  }
}
