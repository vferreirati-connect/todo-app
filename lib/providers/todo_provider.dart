import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:todo_app/models/todo.dart';
import 'package:todo_app/providers/base_provider.dart';

class TodoProvider extends BaseProvider {
  Database db;

  Future open() async {
    String databasePath = await getDatabasesPath();
    String path = join(databasePath, databaseName);
    db = await openDatabase(path, version: 1,
        onCreate: (database, version) async {
          await database.execute(Todo.createTableScript);
        });
  }

  Future<Todo> insert(Todo todo) async {
    if(db == null) {
      await open();
    }
    todo.id = await db.insert(Todo.tableName, todo.toMap());
    return todo;
  }

  Future<List<Todo>> getAll() async {
    if(db == null) {
      await open();
    }
    var queryColumns = [
      Todo.columnId,
      Todo.columnTitle,
      Todo.columnDescription,
      Todo.columnDone
    ];

    List<Map> data = await db.query(
        Todo.tableName,
        columns: queryColumns
    );

    var results = List<Todo>();
    if(data.length > 0) {
      data.forEach( (map) => results.add(Todo.fromMap(map)));
    }

    return results;
  }

  Future<Todo> getById(int id) async {
    if(db == null) {
      await open();
    }
    var queryColumns = [
      Todo.columnId,
      Todo.columnTitle,
      Todo.columnDescription,
      Todo.columnDone
    ];

    List<Map> data = await db.query(
        Todo.tableName,
        columns: queryColumns,
        where: "${Todo.columnId} = ?",
        whereArgs: [id]
    );

    return data.length > 0
        ? Todo.fromMap(data.first)
        : null;
  }

  Future<int> delete(int id) async {
    if(db == null) {
      await open();
    }
    int amount = await db.delete(
        Todo.tableName,
        where: "${Todo.columnId} = ?",
        whereArgs: [id]
    );

    return amount;
  }

  Future<int> update(Todo todo) async {
    if(db == null) {
      await open();
    }

    int amount = await db.update(
      Todo.tableName,
      todo.toMap(),
      where: "${Todo.columnId} = ?",
      whereArgs: [todo.id]
    );

    return amount;
  }

  Future close() async => db.close();
}
