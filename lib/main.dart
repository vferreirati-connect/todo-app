import 'package:flutter/material.dart';
import 'package:todo_app/bloc/editor_page_bloc.dart';
import 'package:todo_app/bloc/list_page_bloc.dart';
import 'package:todo_app/pages/editor_page.dart';
import 'package:todo_app/pages/list_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "My To-dos",
      theme: ThemeData(
        primaryColor: Colors.deepPurple,
        accentColor: Colors.deepOrange
      ),
      routes: {
        // List page is the home page of this app
        ListPage.routeName: (context) => BlocProvider(bloc: ListPageBloc(), child: ListPage()),
        EditorPage.routeName: (context) => BlocProvider(bloc: EditorPageBloc(), child: EditorPage())
      
      },
      onGenerateRoute: (settings) {
       final List<String> routeElements = settings.name.split("/");
       print(routeElements);

       if(routeElements[0] != "") {
         return null;
       }

       if(routeElements[1] == "editor") {
         final id = int.parse(routeElements[2]);
         return MaterialPageRoute(builder: (context) => BlocProvider(bloc: EditorPageBloc(todoId: id), child: EditorPage()));
       }

       return null;
      
      },
      onUnknownRoute: (settings) {
        print("Tried to access invalid route: ${settings.name}");
        return MaterialPageRoute(builder: (context) => BlocProvider(bloc: ListPageBloc(), child: ListPage()));
      },
    );
  }
}