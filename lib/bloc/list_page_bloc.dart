import 'package:bloc/bloc.dart';
import 'package:todo_app/bloc/list_page_state.dart';
import 'package:todo_app/providers/todo_provider.dart';

abstract class ListPageEvent {}

class LoadTodos extends ListPageEvent {}
class TestInsert extends ListPageEvent {}
class ToggleDone extends ListPageEvent {
  final int todoId;
  ToggleDone(this.todoId);
}
class DeleteTodo extends ListPageEvent {
  final int todoId;
  DeleteTodo(this.todoId);
}

class ListPageBloc extends Bloc<ListPageEvent, ListPageState> {
  TodoProvider _todoProvider;

  ListPageBloc() {
    _todoProvider = TodoProvider();
    _init();
  }

  void _init() => dispatch(LoadTodos());
  void onTest() => dispatch(TestInsert());
  void onToggleFavorite(int id) => dispatch(ToggleDone(id));
  void onDelete(int todoId) => dispatch(DeleteTodo(todoId));

  @override
  ListPageState get initialState => ListPageState.empty();

  @override
  Stream<ListPageState> mapEventToState(currentState, event) async* {
    if(event is LoadTodos) {
      yield await loadTodos();

    } else if(event is ToggleDone) {
      yield await toggleDone(event.todoId);

    } else if(event is DeleteTodo) {
      yield await deleteTodo(event.todoId);

    } else {
      throw Exception("Unknown Event");
    }
  }

  Future<ListPageState> loadTodos() async {
    // Load todos and yield a new state
    var todos = await _todoProvider.getAll();
    var newState = ListPageState.empty();
    newState.todos = todos;
    newState.isLoading = false;

    return newState;
  }

  Future<ListPageState> toggleDone(int todoId) async {
    var todo = currentState.todos.where((t) => t.id == todoId).first;
    todo.done = !todo.done;
    _todoProvider.update(todo);

    var newState = ListPageState.empty();
    newState.todos = currentState.todos;
    newState.isLoading = currentState.isLoading;

    return newState;
  }

  Future<ListPageState> deleteTodo(int todoId) async {
    _todoProvider.delete(todoId);

    currentState.todos.removeWhere((t) => t.id == todoId);
    var newState = ListPageState.empty();
    newState.todos = currentState.todos;
    newState.isLoading = currentState.isLoading;

    return newState;
  }

  @override
  void dispose() {
    super.dispose();
    _todoProvider.close();
  }
}
