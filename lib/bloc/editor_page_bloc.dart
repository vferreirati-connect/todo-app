import 'package:bloc/bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:todo_app/bloc/editor_page_state.dart';
import 'package:todo_app/models/todo.dart';
import 'package:todo_app/providers/todo_provider.dart';

abstract class EditorPageEvent {}
class SaveTodo extends EditorPageEvent {}
class UpdateTodo extends EditorPageEvent {}
class NotifyEditor extends EditorPageEvent {
  Todo todo;
  NotifyEditor(this.todo);
}
class UpdateTitle extends EditorPageEvent {
  String title;
  UpdateTitle(this.title);
}
class UpdateDescription extends EditorPageEvent {
  String description;
  UpdateDescription(this.description);
}

class EditorPageBloc extends Bloc<EditorPageEvent, EditorPageState> {
  TodoProvider _todoProvider;
  int todoId;
  Todo updatingTodo;
  BehaviorSubject<Todo> _editingController = BehaviorSubject();
  Observable<Todo> get editingTodo => _editingController.stream;

  bool get isEditingMode => todoId != null;

  EditorPageBloc({this.todoId}) {
    _todoProvider = TodoProvider();

    if(isEditingMode) {
      initData();
    }
  }

  void initData() async {
    updatingTodo = await _todoProvider.getById(todoId);
    _editingController.add(updatingTodo);
  }

  @override
  get initialState => EditorPageState.empty();

  void onUpdateTitle(String value) => dispatch(UpdateTitle(value));
  void onUpdateDescription(String value) => dispatch(UpdateDescription(value));
  Future<bool> onSave() async {
    // Check for validation erros
    // If none found, save the object on database
    if(currentState.todoTitleError == null) {
      if(isEditingMode) {
        dispatch(UpdateTodo());

        var todo = new Todo(
          id: todoId,
          title: currentState.todoTitle,
          description: currentState.todoDescription,
          done: updatingTodo.done
        );
        var amount = await _todoProvider.update(todo);
        return amount > 0;

      } else {
        dispatch(SaveTodo());

        var todo = new Todo(
            title: currentState.todoTitle,
            description: currentState.todoDescription,
            done: false
        );
        todo = await _todoProvider.insert(todo);
        return todo.id != null;
      }
    }

    return false;
  }

  @override
  Stream<EditorPageState> mapEventToState(currentState, EditorPageEvent event) async* {
    if(event is UpdateTitle) {
      var validTitle = event.title.trim().isNotEmpty;
      if(!validTitle) {
        var newState = EditorPageState(
          event.title,
          "A title is required",
          currentState.todoDescription,
          currentState.isSaving
        );
        yield newState;
      } else {
        var newState = EditorPageState(
          event.title,
          null,
          currentState.todoDescription,
          currentState.isSaving
        );
        yield newState;
      }

    } else if(event is UpdateDescription) {
      var newState = EditorPageState(
          currentState.todoTitle,
          currentState.todoTitleError,
          event.description,
          currentState.isSaving
      );
      yield newState;

    } else if(event is NotifyEditor) {
      var todo = event.todo;
      var newState = EditorPageState(
        todo.title,
        null,
        todo.description,
        false
      );

      yield newState;

    } else if(event is SaveTodo || event is UpdateTodo) {
      if(currentState.isSaving) {
        return;
      } else {
        var newState = EditorPageState(
          currentState.todoTitle,
          currentState.todoTitleError,
          currentState.todoDescription,
          true
        );

        yield newState;
      }
    } else {
      throw Exception("Unknown Event");
    }
  }

  @override
  void dispose() {
    super.dispose();
    _editingController.close();
  }
}