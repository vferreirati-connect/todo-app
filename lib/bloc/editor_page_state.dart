class EditorPageState {
  String todoTitle;
  String todoTitleError;
  String todoDescription;

  bool isSaving;

  EditorPageState(this.todoTitle, this.todoTitleError, this.todoDescription, this.isSaving);

  EditorPageState._();
  factory EditorPageState.empty() {
    return EditorPageState("", null, "", false);
  }
}