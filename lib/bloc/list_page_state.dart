import 'package:todo_app/models/todo.dart';

class ListPageState {
  List<Todo> todos;
  bool isLoading;

  ListPageState._();
  factory ListPageState.empty() {
    var state = ListPageState._();
    state.todos = new List();
    state.isLoading = true;

    return state;
  }
}