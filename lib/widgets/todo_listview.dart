import 'package:flutter/material.dart';
import 'package:todo_app/models/todo.dart';
import 'package:todo_app/widgets/todo_card.dart';

class TodoListView extends StatelessWidget {
  final List<Todo> todos;

  TodoListView(this.todos);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: todos.length,
      itemBuilder: (context, position) => TodoCard(todos[position]),
    );
  }
}