import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:path/path.dart';
import 'package:todo_app/bloc/list_page_bloc.dart';
import 'package:todo_app/models/todo.dart';
import 'package:todo_app/pages/editor_page.dart';

class TodoCard extends StatelessWidget {
  final Todo todo;

  TodoCard(this.todo);

  Widget _titleText() {
    return Text(
      todo.title,
      style: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 18
      )
    );
  }

  Widget _descriptionText() {
    if(todo.description == null) {
      return Container();
    }

    return Column(
      children: <Widget>[
        SizedBox(height: 5),
        Text(
          todo.description,
          maxLines: 4,
          overflow: TextOverflow.ellipsis,
        )
      ],
    );
  }

  Widget _buttonBar(BuildContext context, ListPageBloc bloc) {
    return ButtonBar(
      children: <Widget>[
        _editButton(context),
        _doneBadge(bloc)
      ],
    );
  }

  Widget _editButton(BuildContext context) {
    return IconButton(
      iconSize: 25,
      icon: Icon(Icons.edit,),
      color: Theme.of(context).primaryColor,
      onPressed: () => _onEdit(context),
    );
  }

  Widget _doneBadge(ListPageBloc bloc) {
    return IconButton(
      iconSize: 25,
      icon: todo.done ? Icon(Icons.check_circle) : Icon(Icons.check_circle_outline),
      color: todo.done ? Colors.green : Colors.red,
      onPressed: () => bloc.onToggleFavorite(todo.id),
    );
  }

  void _onDismiss(DismissDirection direction, ListPageBloc bloc) {
    bloc.onDelete(todo.id);
  }

  void _onEdit(BuildContext context) {
    var routeName = "${EditorPage.routeName}/${todo.id}";
    Navigator.of(context).pushNamed(routeName);
  }

  @override
  Widget build(BuildContext context) {
    ListPageBloc bloc = BlocProvider.of<ListPageBloc>(context);

    return Dismissible(
      key: Key(todo.id.toString()),
      child: Card(
        margin: EdgeInsets.all(10),
        child: Container(
          padding: EdgeInsets.only(top: 5, left: 5, right: 5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _titleText(),
              _descriptionText(),
              _buttonBar(context, bloc)
            ],
          ),
        )
      ),
      onDismissed: (direction) => _onDismiss(direction, bloc),
    );
  }
}
