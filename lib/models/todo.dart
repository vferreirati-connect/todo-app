class Todo {
  static String tableName = "tb_todo";
  static String columnId = "id";
  static String columnTitle = "title";
  static String columnDescription = "description";
  static String columnDone = "done";
  static String createTableScript =
      "CREATE TABLE $tableName "
      "($columnId INTEGER PRIMARY KEY AUTOINCREMENT,"
      "$columnTitle TEXT NOT NULL,"
      "$columnDescription TEXT,"
      "$columnDone INTEGER NOT NULL"
      ")";

  int id;
  String title;
  String description;
  bool done;

  Todo({this.id, this.title, this.description, this.done});
  factory Todo.fromMap(Map<String, dynamic> map) {
    return Todo(
      id: map[columnId],
      title: map[columnTitle],
      description: map[columnDescription],
      done: map[columnDone] == 1
    );
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic> {
      columnTitle: title,
      columnDescription: description,
      columnDone: (done) ? 1 : 0
    };
    if(id != null) {
      map[columnId] = id;
    }

    return map;
  }
}